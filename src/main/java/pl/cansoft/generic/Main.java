package pl.cansoft.generic;

abstract class Friut {}

class Orange extends Friut {
    void orange() {
        System.out.println("I am orange");
    }
}
class Apple extends Friut {
    void apple() {
        System.out.println("I am apple");
    }
}
class Strawbery extends Friut {
    void strawbery() {
        System.out.println("I am strawbery");
    }
}

class Box<T extends Friut> { // Box<T> dla dowolnego obiektu, Box<T extends Friut> dla obiektów dziedziczących z Friut
    T value;
    Box(T value) {
        this.value = value;
    }
}

public class Main {
    public static void main(String[] args) {

        Orange orange = new Orange();
        Apple apple = new Apple();
        Strawbery strawbery = new Strawbery();

        Box<Orange> box1 = new Box<Orange>(orange); // wymusic tu tylko Orange !
        box1.value.orange();

        doSometnig(box1);

        Box<Apple> box2 = new Box<>(apple); // Box<Apple> nie wymagany, może byc Box<> (kompilator sie domysli)
        box2.value.apple();

        doSometnig(box2);

        Box<Strawbery> box3 = new Box<Strawbery>(strawbery);
        box3.value.strawbery();

        doSometnig(box3);
    }

    private static void doSometnig(Box<? extends Friut> box) {
    }
}
