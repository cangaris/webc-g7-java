package pl.cansoft.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import pl.cansoft.equals.Car;

public class Main {
    public static void main(String[] args) {

        List<Integer> numbers = Arrays.asList(1, 2, 3, 4);
        Consumer<Integer> consumer = n -> System.out.println(n);
        numbers.forEach(consumer);

        ArrayList<Integer> ints = new ArrayList<Integer>();
        ints.add(1);
        ints.add(2);
        ints.add(3);
        ints.add(4);

        Predicate<Integer> filterFn = x -> x % 2 == 0;
        UnaryOperator<Integer> mapFn = x -> x * 3; // Function<Integer, Integer>

        List<Integer> results1 = ints.stream()
            .filter(filterFn)
            .map(mapFn)
            .toList();

        System.out.println(results1);

        // ArrayList(10)
        // super do odczytywania wartości po indexie, słaba w dodawaniu nowych rzeczy, super w edycji, słaba w usuwaniu

        // LinkedList
        // dodawanie i usuwanie bardzo łatwe, słaba dla szukania po indexie i edycji

        var arrayList = new ArrayList<String>(); // impl interface List (10)
        var linkedList = new LinkedList<String>(); // impl interface List
        // 10 elementow w pamieci

        arrayList.add("Damian"); // 0
        arrayList.add("Damian"); // 1
        arrayList.add("Damian"); // 2

        var hasSet = new HashSet<Car>();

        hasSet.add(new Car(2, "Mercedes"));
        System.out.println(new Car(2, "Mercedes").hashCode());

        hasSet.add(new Car(2, "Mercedes"));
        System.out.println(new Car(2, "Mercedes").hashCode());

        hasSet.add(new Car(2, "Mercedes"));
        System.out.println(new Car(2, "Mercedes").hashCode());

        System.out.println(arrayList);
        System.out.println(hasSet);

        HashMap<String, Car> otherPairsMap = new HashMap<>();
        otherPairsMap.put("a", new Car(2, "Mercedes"));
        otherPairsMap.put("b", new Car(2, "Mercedes"));
        otherPairsMap.put("a", new Car(2, "Mercedes"));

        System.out.println(otherPairsMap);

        HashMap<String, String> sampleMap = new HashMap<>();
        sampleMap.put("firstName", "Magda");
        sampleMap.put("lastName", "Kowalska");

        sampleMap.get("firstName"); // Magda
    }
}
