package pl.cansoft.calculator;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Calculator {

    static Scanner inputScanner = new Scanner(System.in);

    public static void main(String[] args) {
        // typ prosty -> int, boolean (to nie są obiekty)
        // typy złożone są obiektami -> obiekt typu Integer, String
        System.out.print("""
            MENU:
            [A] dodawanie
            [B] odejmowanie
            [C] mnożenie
            [D] dzielenie
            [E] potęgowanie
            [F] pierwiastkowanie
            """);

        System.out.print("Podaj typ operacji: ");
        String operationType = inputScanner.next();
        operationType = operationType.toUpperCase();

        if ("A".equals(operationType)) { // Objects.equals(operationType, "A")
            Double a = getData(1);
            if (a == null) {
                System.out.println("Otrzymana wartość jest zabroniona!");
                return;
            }
            Double b = getData(2);
            if (b == null) {
                System.out.println("Otrzymana wartość jest zabroniona!");
                return;
            }
            double result = a + b;
            showResult(result);
        } else if ("B".equals(operationType)) {
            double a = getData(1);
            double b = getData(2);
            double result = a - b;
            showResult(result);
        } else if ("C".equals(operationType)) {
            double a = getData(1);
            double b = getData(2);
            double result = a * b;
            showResult(result);
        } else if ("D".equals(operationType)) {
            double a = getData(1);
            double b = getData(2);
            double result = a / b;
            showResult(result);
        } else if ("E".equals(operationType)) {
            double a = getData(1);
            double b = getData(2);
            double result = Math.pow(a, b);
            showResult(result);
        } else if ("F".equals(operationType)) {
            double a = getData(1);
            double b = getData(2);
            double result = Math.pow(a, 1 / b); // Math.sqrt
            showResult(result);
        } else {
            System.out.println("Nieznany typ operacji!");
        }
    }

    static void showResult(double result) {
        String text = String.format("Wynik: %s", result);
        System.out.println(text);
    }

    static Double getData(int digitNumer) {
        String text = String.format("Podaj liczbę %s: ", digitNumer);
        System.out.print(text);
        try {
            return inputScanner.nextDouble();
        } catch (InputMismatchException exception) {
            return null;
        }
    }
}
