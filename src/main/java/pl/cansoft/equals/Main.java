package pl.cansoft.equals;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car(1, "Ford");
        Car car2 = new Car(1, "Ford");

        System.out.println(car1 == car2); // ref

        System.out.println(car1.equals(car2));
    }
}
