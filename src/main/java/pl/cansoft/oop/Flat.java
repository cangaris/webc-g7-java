package pl.cansoft.oop;

public class Flat implements Windows {
    private Integer windowsNumber;

    public Flat(Integer windowsNumber) {
        this.windowsNumber = windowsNumber;
    }

    public void setWindowsNumber(Integer windowsNumber) {
        this.windowsNumber = windowsNumber;
    }

    public Integer getWindowsNumber() {
        return windowsNumber;
    }
}
