package pl.cansoft.oop.enums;

public class Main {
    public static void main(String[] args) {
        Gender gender = Gender.FEMALE;

        System.out.println(gender);
        System.out.println(Size.S.getWidth());
        System.out.println(Size.XS.getWidth());
        System.out.println(Size.M.getHeight());
    }
}
