package pl.cansoft.oop.enums;

public enum Size {
    XS(50, 40),
    S(60, 50),
    M(70, 60),
    L(80, 70),
    XL(90, 80),
    XXL(100, 90);

    final int width;
    final int height;
    Size(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
