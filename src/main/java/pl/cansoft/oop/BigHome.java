package pl.cansoft.oop;

public class BigHome extends Home {

    private Integer dorNumber;

    BigHome(Integer dorNumber, Integer windowsNumber) {
        super(windowsNumber); // Home(Integer windowsNumber)
        this.dorNumber = dorNumber;
    }

    // nadpisana metoda getWindowsNumber (nie korzystamy getWindowsNumber z klasy Home, tylko getWindowsNumber z klasy BigHome)
    public Integer getWindowsNumber() {
        return super.getWindowsNumber() + 2;
    }
}
// Animal -> Cat -> Tiger -> WhiteTiger
// Run, Fly, Swim, Walk, Eat
// class Fish implements Swim, Eat
// class FlyingFish extends Fish implements Fly
// class Human implements Run, Walk, Swim, Eat
// class Flower
