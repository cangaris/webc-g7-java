package pl.cansoft.oop;

public class Main {
    public static void main(String[] args) {

        Car car = new Car();
        car.run(); // print pojazd jedzie

        Home home1 = new Home(10); // a5
        Home home2 = new Home(9); // a7
        Home home3 = home2;
        Home home4 = home3;

        Home home5 = home4;
        clean(home1);

        BigHome bigHome = new BigHome(3, 20);
        clean(bigHome);

        System.out.println("Ilość okien dla bigHome: " + bigHome.getWindowsNumber()); // Ilość okien 8

        Flat flat = new Flat(4);
        clean(flat);

        home5.setWindowsNumber(8);
        home5.cleanWindows(); // Rozpoczynam mycie okien + Zakończyłem mycie okien

        System.out.println("Ilość okien dla home1: " + home1.getWindowsNumber()); // Ilość okien 10
        System.out.println("Ilość okien dla home2: " + home2.getWindowsNumber()); // Ilość okien 8
        System.out.println("Ilość okien dla home3: " + home3.getWindowsNumber()); // Ilość okien 8
        System.out.println("Ilość okien dla home4: " + home4.getWindowsNumber()); // Ilość okien 8
        System.out.println("Ilość okien dla home5: " + home5.getWindowsNumber()); // Ilość okien 8
        System.out.println("Ilość okien dla flat: " + flat.getWindowsNumber()); // Ilość okien 8
    }

    static void clean(Windows windows) {
        windows.cleanWindows();
        // Home, BigHome i Flat mogą być paranetrem metody bo wszystkie implementując wspólny interface Windows
    }
}
