package pl.cansoft.oop;

public interface Windows {

    void setWindowsNumber(Integer integer);

    Integer getWindowsNumber();

    default void cleanWindows() {
        startCleanWindows();
        endCleanWindows();
    }

    private void startCleanWindows() {
        System.out.println("Rozpoczynam mycie okien");
    }

    private void endCleanWindows() {
        System.out.println("Zakończyłem mycie okien");
    }
}
