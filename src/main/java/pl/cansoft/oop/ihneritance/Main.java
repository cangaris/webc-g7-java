package pl.cansoft.oop.ihneritance;

abstract class Vehicle { // extends Object (każdy obiekt w Javie niejwnie dziedziczy po klasie Object)
    Integer wheels;
    Vehicle(Integer wheels) {
        this.wheels = wheels;
    }

    abstract void showVehicleName();

    public String toString() {
        return "Vehicle{" +
            "wheels=" + wheels +
            '}';
    }
}

class Car extends Vehicle {
    String color;
    Car(String color, Integer wheels) {
        super(wheels);
        this.color = color;
    }

    public final void setColor(String color) { // metoda final oznacza, że nie można jej napisać w kasie potomnej
        this.color = color;
    }

    void showVehicleName() { // napisana metoda abstract z klasy Vehicle
        System.out.println("Car");
    }

    public String toString() {
        return "Car{" +
            "color='" + color + '\'' +
            ", wheels=" + wheels +
            "} ";
    }
}

final class SUV extends Car { // final class - nie można dziedziczyc po klasie SUV
    Integer passengers;
    SUV(Integer passengers, String color, Integer wheels) {
        super(color, wheels);
        this.passengers = passengers;
    }

    void showVehicleName() { // napisana metoda abstract z klasy Car
        System.out.println("SUV");
    }

    public String toString() {
        return "SUV{" +
            "passengers=" + passengers +
            ", color='" + color + '\'' +
            ", wheels=" + wheels +
            "} ";
    }
}

public class Main {
    public static void main(String[] args) {
        // Vehicle vehicle = new Vehicle(2); // nie można tworzyć obiektu z klasy abstract, ona jest tylko pod dziedziczenie
        final Car car = new Car("red", 4); // utworzenie stałej

        SUV suv = new SUV(5, "blue", 4);

        // System.out.println(vehicle); // toString() wywołany niejawnie
        System.out.println(car);
        System.out.println(suv);

        car.setColor("white");
        suv.setColor("brown");

        System.out.println(car);
        System.out.println(suv);

        car.showVehicleName();
        suv.showVehicleName();
    }
}
