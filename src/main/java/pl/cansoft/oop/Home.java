package pl.cansoft.oop;

public class Home implements Windows {
    private Integer windowsNumber;

    public Home(Integer windowsNumber) {
        this.windowsNumber = windowsNumber;
    }

    public Integer getWindowsNumber() {
        return windowsNumber;
    }

    public void setWindowsNumber(Integer windowsNumber) {
        this.windowsNumber = windowsNumber;
    }
}
