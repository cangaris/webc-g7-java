package pl.cansoft.stream;

import java.util.List;
import java.util.stream.Collectors;

enum Gender {
    FEMALE,
    MALE
}

class User {
    String firstName;
    String lastName;
    Gender gender;

    public User(String firstName, String lastName, Gender gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "User{" +
            "firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", gender=" + gender +
            '}';
    }
}

public class Main3 {
    public static void main(String[] args) {

        var users = List.of(
            new User("Halina", "Kiepska", Gender.FEMALE),
            new User("Ania", "Nowak", Gender.FEMALE),
            new User("Adam", "Kowalski", Gender.MALE),
            new User("Tomasz", "Nowak", Gender.MALE),
            new User("Basia", "Kowalska", Gender.FEMALE),
            new User("Zosia", "Nowak", Gender.FEMALE),
            new User("Kamil", "Kiepski", Gender.MALE)
        );
        // 1. wyciągnąć tylko kobiety - List(User)
        var onlyFemale = users.stream()
            .filter(user -> user.gender == Gender.FEMALE) // user.firstName.endsWith("a")
            .collect(Collectors.toUnmodifiableList()); // alternatywa do .toList();
        System.out.println(onlyFemale);

        // 2. wyciągnąć Kowalskich - List(User)
        var onlySurnameKowalski = users.stream()
            .filter(user -> user.lastName.startsWith("Kowalsk"))
            .toList(); // lista niemutowalna
        System.out.println(onlySurnameKowalski);

        // 3. wyciągnąć wszystkich oprócz Kiepskich - List(User)
        var allSurnamesExceptKiepski = users.stream()
            .filter(user -> !user.lastName.startsWith("Kiepsk"))
            .collect(Collectors.toList()); // gdyby lista miała byc mutowalna
        System.out.println(allSurnamesExceptKiepski);

        // 4. ile mamy kobiet (int)
        long femaleCount = users.stream()
            .filter(user -> user.gender.equals(Gender.FEMALE))
            .count();
        System.out.println("Kobiet jest: " + femaleCount);

        // 5. czy liście jest jakaś kobieta? (boolean)
        boolean hasAnyFemale = users.stream()
            .anyMatch(user -> user.gender.equals(Gender.FEMALE));
        System.out.println("Czy są kobiety: " + (hasAnyFemale ? "TAK" : "NIE"));

        // 6. czy w liście są sami mężczyźni? (boolean)
        boolean onlyMale = users.stream()
            .allMatch(user -> user.gender.equals(Gender.MALE));
        System.out.println("Czy sami mezczyzni: " + (onlyMale ? "TAK" : "NIE"));

        // 7. czy w liście nie ma mężczyzn? (boolean)
        boolean noneMale = users.stream()
            .noneMatch(user -> user.gender.equals(Gender.MALE));
        System.out.println("Czy nie ma mezczyzn: " + (noneMale ? "TAK" : "NIE"));

        users.stream()
            .filter(user -> isMale(user))
            .skip(1) // skip pomija x elementow z poczatku, limit bierze x elementow z poczatku
            .forEach(user -> saveToDatabase(user));
    }

    static boolean isMale(User user) {
        return !user.firstName.endsWith("a");
    }

    static void saveToDatabase(User user) {
        System.out.println("zapisuje do bazy: " + user);
    }
}
